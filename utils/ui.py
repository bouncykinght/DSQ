import sys
sys.path.insert(0, sys.path[0]+"/../")  # sys.path.append("..")好像没有用,
                                        # 或者像yolov5那样
                                        # from pathlib import Path
                                        # FILE = Path(__file__).resolve()
                                        # ROOT = FILE.parents[3]  # YOLOv5 root directory
                                        # if str(ROOT) not in sys.path:
                                        #     sys.path.append(str(ROOT))  # add ROOT to PATH

import tkinter as tk
from tkinter import ttk
from PIL import Image, ImageTk
from utils.Get_img import get_img
import cv2

# Window已经继承了tk.Tk,并且初始化了父类，那么本身self就是tk.Tk了。self = tk.Tk()
class Window(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("戴森球量化工具")
        self.geometry(str(int(self.winfo_screenwidth() / 2)) + "x" + str(int(self.winfo_screenheight() / 2)))
        self.resizable(0, 0)

        self._init_label()
        self.quantity_per_minute_spinbox, self.quantity_per_minute_dstr = self._init_spinbox(100, 10, "60")
        temp_lsit = ["选择设备数量"]
        for i in range(1, 200):
            temp_lsit.append("需要" + str(i) + "个设备")
        self.number_of_equipment_cbox = self._init_combobox(200, 10, 12, temp_lsit)
        self.button = self._init_button(350, 2, image_name = "图标")
        self.add_button = self._init_button(420, 10, text = "增加需求", width = 30)
        self.parameter_settings_button = self._init_button(680, 10, text = "参数设置", width = 10)

        self.hide_raw_materials_check, self.hide_raw_materials_Var = self._init_checkbutton(10, 40, "隐藏原料")
        temp_lsit = ["制作台Mk.I", "制作台Mk.II", "制作台Mk.III", "重组式制造台"]
        self.production_stage_cbox = self._init_combobox(100, 40, 12, temp_lsit)
        temp_lsit = ["电弧熔炉", "位面熔炉", "负熵熔炉"]
        self.furnace_cbox = self._init_combobox(230, 40, 11, temp_lsit)
        temp_lsit = ["化工厂", "量子化工厂"]
        self.chemical_plant_cbox = self._init_combobox(360, 40, 11, temp_lsit)
        temp_lsit = ["增产剂Mk.I", "增产剂Mk.II", "增产剂Mk.III"]
        self.yield_increasing_agent_cbox = self._init_combobox(480, 40, 11, temp_lsit)
        temp_lsit = ["无", "加速", "增产"]
        self.effect_of_yield_increasing_agent_cbox = self._init_combobox(600, 40, 5, temp_lsit)
        temp_lsit = ["矩阵研究站", "自演化研究站"]
        self.research_station_cbox = self._init_combobox(670, 40, 11, temp_lsit)

    def _init_label(self):
        label1 = tk.Label(self, font = ('宋体',12), text = "每分钟数量：")
        label1.place(x=10, y=10)
        label2 = tk.Label(self, font = ('宋体',12), text = "或者：")
        label2.place(x=160, y=10)

    def _init_spinbox(self, x, y, default_text, width=5):
        dstr = tk.StringVar()
        dstr.set(default_text)
        spinbox = tk.Spinbox(self, font = ('宋体',12), textvariable = dstr, width = width, from_= 0, to=9999999999999999999999999, increment = 1)
        spinbox.place(x = x, y = y)
        return spinbox, dstr

    def _init_combobox(self, x, y, width=5, values = ["None"]):
        cbox = ttk.Combobox(self, font = ('宋体',12), width = width, values = values)
        cbox.current(0)
        cbox.place(x = x, y = y)
        return cbox

    def _init_button(self, x, y, width=5, text = None, image_name = None, command = None):
        # https://blog.csdn.net/m0_50000839/article/details/107920494
        if text == None and image_name == None:
            return None
        if text != None and image_name == None:
            button = tk.Button(self, font = ('宋体',12), text = text, width = width, command = command)
            button.place(x = x, y = y)
            return button
        if text == None and image_name != None:
            paned = tk.PanedWindow(self)
            paned.pack(fill = tk.X, side = tk.LEFT)
            img = get_img(image_name)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            im = Image.fromarray(img)
            paned.photo = ImageTk.PhotoImage(image = im)
            button = tk.Button(self, image = paned.photo, command = command)
            button.place(x = x, y = y)
            return button
        return None
    
    def _init_checkbutton(self, x, y, text):
        CheckVar = tk.IntVar()
        check = tk.Checkbutton(self, font = ('宋体',12), text = text, variable = CheckVar, onvalue = 1, offvalue = 0)
        check.place(x = x, y = y)
        return check, CheckVar


if __name__ == "__main__":
    window = Window()
    # while 1:
    #     print(window.quantity_per_minute_dstr.get())
    #     window.update()
    window.mainloop()
