import sys
from typing import Any
sys.path.insert(0, sys.path[0]+"/../")

import utils.goods
import utils.MultiTreeNode

class compute():
    def __init__(self, json_path = "data/goods.json"):
        self.goods = utils.goods.goods_base.load_good_json(json_path)
        self.energy = utils.goods.goods_base.load_good_energy()
        self.space = utils.goods.goods_base.load_good_space()

    def __call__(self, out_name, out_num):
        self._computer(out_name, out_num)

    def _computer(self, out_name, out_num):
        out_plan = [(out_name, out_num)]
        while out_plan:
            out_lsit = []
            good = out_plan.pop(0)
            print(good)
            for item in self.goods:# 寻找配方
                s_list = item.s
                for s in s_list:
                    if s["name"] == good[0]:
                        out_lsit.append(item)
            chose_out = out_lsit[0]
            t = chose_out.t
            for s in chose_out.s:
                if s["name"] == good[0]:
                    if 'n' in s:
                        s_n = s["n"]
                    else:
                        s_n = 1
                    for q in chose_out.q:
                        out_plan.append((q["name"], (q["n"] * good[1]) / (s_n)))

if __name__ == "__main__":
    com = compute()
    com(out_name = "红矩阵", out_num = 60)