import json
class goods_base():
    def __init__(self, s, m, q, t):
        self.s = s
        self.group = None
        self.m = m
        self.q = q
        self.t = t
        self.noExtra = None

    def __str__(self):
        return f"产物:{self.s} 生产设备:{self.m} 需求产物:{self.q} 时间:{self.t}"

    @staticmethod
    def load_good_json(json_path):
        goods_list = []
        with open(json_path, 'r', encoding='utf-8') as f:
            data = json.load(f)
            for item in data:
                goods = goods_base(item['s'], item['m'], item['q'], item['t'])
                if "groups" in item:
                    goods.group = item['group']
                if "noExtra" in item:
                    goods.noExtra = item['noExtra']
                goods_list.append(goods)
        return goods_list

    @staticmethod
    def load_good_energy():
        energy_data = {}
        energy_data["研究站"] = 0.48
        energy_data["制作台Mk.Ⅰ"] = 0.27
        energy_data["制作台Mk.Ⅱ"] = 0.54
        energy_data["制作台Mk.Ⅲ"] = 1.08
        energy_data["重组式制造台"] = 2.7
        energy_data["电弧熔炉"] = 0.36
        energy_data["位面熔炉"] = 1.44
        energy_data["负熵熔炉"] = 2.88
        energy_data["矿脉"] = 0.42 / 6
        energy_data["采矿机"] = 0.42
        energy_data["大型采矿机"] = 2.94
        energy_data["原油萃取站"] = 0.84
        energy_data["抽水机"] = 0.3
        energy_data["原油精炼机"] = 0.96
        energy_data["化工厂"] = 0.72
        energy_data["量子化工厂"] = 2.16
        energy_data["粒子对撞机"] = 12
        energy_data["轨道采集器"] = 0
        energy_data["射线接收塔"] = 0
        energy_data["能量枢纽"] = 0
        energy_data["分馏塔"] = 0.72
        return energy_data

    @staticmethod
    def load_good_space():
        space_data = {}
        space_data["研究站"] = 36 / 15
        space_data["制作台Mk.Ⅰ"] = space_data["制作台Mk.Ⅱ"] = space_data["制作台Mk.Ⅲ"] = space_data["重组式制造台"] = 16
        space_data["电弧熔炉"] = space_data["位面熔炉"] = space_data["负熵熔炉"] = 16
        space_data["原油精炼机"] = 28
        space_data["化工厂"] = 35
        space_data["量子化工厂"] = 35
        space_data["射线接收塔"] = 24
        space_data["能量枢纽"] = 64
        space_data["分馏塔"] = 16
        space_data["粒子对撞机"] = 45
        return space_data

class input_goods(goods_base):
    def __init__(self, name, n):
        self.name = name
        self.n = n

    def __str__(self):
        return f"{self.name} {self.n}"

if __name__ == '__main__':
    # 测试代码
    json_path = "data/goods.json"
    goods = goods_base.load_good_json(json_path)
    energy = goods_base.load_good_energy()
    space = goods_base.load_good_space()
    for item in goods:
        print(item)
    for item in energy:
        print(energy[item])
    for item in space:
        print(space[item])
