
class Multi_tree_node:
    def __init__(self, node_value):
        self.value = node_value
        self.children = []

    def add_child(self, child_node):
        self.children.append(child_node)

    def remove_child(self, child_node):
        self.children = [child for child in self.children if child is not child_node]
    
    def traverse(self):
        print(self.value)
        for child in self.children:
            child.traverse()

    def BFS(self):
        queue = [self]
        res = []
        while queue:
            node = queue.pop(0)
            res.append(node.value)
            queue.extend(node.children)
        return res

    def find_node(self, root, value):
        if root.value == value:
            return root
        for child in root.children:
            node = self.find_node(child, value)
            if node:
                return node
        return None

if __name__ == "__main__":
    root = Multi_tree_node("A")
    temp = Multi_tree_node("B")
    root.add_child(temp)
    temp = Multi_tree_node("C")
    root.add_child(temp)
    node = root.find_node(root, "C")
    temp = Multi_tree_node("C")
    node.add_child(temp)
    root.traverse()
    res = root.BFS()
    print(res)